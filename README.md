    1. Database creation and .env file
      

    • In phpMyAdmin → create database → DB_name=Api
      
    • In .env file
      
      DB_CONNECTION=mysql
      DB_HOST=127.0.0.1
      DB_PORT=3306
      DB_DATABASE=Api
      DB_USERNAME=your database user name
      DB_PASSWORD=your database password
      
      API_STANDARDS_TREE=vnd
      API_SUBTYPE=ApiTask
      API_DOMAIN=localhost
      API_VERSION=v1
	      API_NAME="ApiTask"

    • In Terminal → Run → php artisan migrate 
    • for seeding users → Run→  php artisan migrate –seed
      
    2. Get api routes
    • php artisan api:routes
    • php artisan api:routes --versions v1
      
    3. User routes in postman
    • get all users → localhost:8000/users
    • get user by id → localhost:8000/users/id
      
    4. News routes in postman
    • get all news → localhost:8000/news → GET
    • get news by id → localhost:8000/news/id → GET
    • create news →  localhost:8000/news → POST
    • update news →  localhost:8000/news/id → PUT
    • delete news →  localhost:8000/news/id → DELETE
      
    5. Authentication
    • in composer.json
      
           "require": {
                       “tymon/jwt-auth:”^1.0.0-beta.3@dev”
                       }

    • in config/app.php

		“providers”:{
			Tymon\JWTAuth\Providers\LaravelServiceProvider::class, 

				}
		'aliases' => [
          'JWTAuth' => Tymon\JWTAuth\Facades\JWTAuth::class,
		'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class
		]

      
    • php artisan vendor:publish –provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
      
    • php artisan jwt:generate
	
    • in jwt.php
      
    1. 'jwt' => 'Tymon\JWTAuth\Providers\JWT\Namshi',
    2. 'auth' => 'Tymon\JWTAuth\Providers\Auth\Illuminate',
    3. 'storage' => 'Tymon\JWTAuth\Providers\Storage\Illuminate'
           
           
    • user register → localhost:8000/register → and give params (name , email , password , password_confirmation , phone , NID)
    • user login → localhost:8000/login → and give params (name , email , password)
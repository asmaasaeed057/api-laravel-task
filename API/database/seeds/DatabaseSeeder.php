<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create([
            'name' => 'Asmaa saeed',
            'email' => 'asmaa@gmail.com',
            'phone' => '0111746546',
            'NID' => '1945153165416313',
            'password' => bcrypt('password')
        ]);

        User::create([
            'name' => 'mohamed ali',
            'email' => 'mohamed@gmail.com',
            'phone' => '0111746546',
            'NID' => '1945153165416313',
            'password' => bcrypt('password')
        ]);

        User::create([
            'name' => 'John Doe',
            'email' => 'john.doe@gmail.com',
            'phone' => '0112746546',
            'NID' => '1945153165416313',
            'password' => bcrypt('password')
        ]);

        User::create([
            'name' => 'sarah ahmed',
            'email' => 'sarah@gmail.com',
            'phone' => '01141546546',
            'NID' => '1945153165416313',
            'password' => bcrypt('password')
        ]);

        User::create([
            'name' => 'Jason Bourne',
            'email' => 'jason@gmail.com',
            'phone' => '0111746546',
            'NID' => '1945153165416313',
            'password' => bcrypt('jasonB')
        ]);
    }
}

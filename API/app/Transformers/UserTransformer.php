<?php

namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'            => (int) $user->id,
            'name'     => $user->name,
            'email'      => $user->email,
            'phone'         =>$user->phone,
            'NID'   =>$user->NID,
            'email_verified_at' => $user->email_verified_at,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at
        ];
    }

}
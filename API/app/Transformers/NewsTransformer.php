<?php
namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use App\News;

class NewsTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(News $news)
    {
        return [
            'id'            => (int) $news->id,
            'title'     => $news->title,
            'description'      => $news->description,
            'writer_id'    => $news->writer_id
        ];
    }

}
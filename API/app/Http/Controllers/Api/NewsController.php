<?php

namespace App\Http\Controllers\Api;

use App\News;
use App\Transformers\NewsTransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\NotFoundHttpException;

class NewsController extends BaseController

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return $this->response->collection($news, new NewsTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $rules = [
            'title' => 'required|alpha_num:App\News,title',
            'description' => 'required|min:10:App\News,description',
            'writer_id' => 'numeric:App\News,writer_id'
        ];

        $payload = app('request')->only('title', 'description');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new News.', $validator->errors());
        }
        else{
         
            News::create([
                'title' =>$request->title,
                'description' =>$request->description,
                'writer_id' =>$request->writer_id
            ]);

            return $this->response->created();
        }

        //    $news = new News();
        //     $news->title = $request['title'];
        //     $news->description = $request['description'];
        //     $news->writer_id = $request['writer_id'];
        //     $news->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */

    public function show($newsId)
    {
        $meta = array(
            'message' => 'get news success',
            'status_code' => 200
        );

        $news = News::findOrFail($newsId);

        if (!$news)
        {
            throw new NotFoundHttpException;
        }
        return $this->response->item($news, new NewsTransformer)->setMeta($meta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $newsId)
    {
        
        $news = News::find($newsId);

        if (!$news)
        {
            throw new UpdateResourceFailedException('you are updating non existing News');
        }

        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->writer_id = $request->input('writer_id');    
        $news->save();

        return $this->response->created();
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($newsId)
    {
        $news = News::find($newsId);
        if (!$news){
            throw new DeleteResourceFailedException('you are deleting non existing News');
        }
        News::findOrFail($newsId)->delete();
    }
}

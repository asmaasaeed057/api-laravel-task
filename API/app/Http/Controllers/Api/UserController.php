<?php

namespace App\Http\Controllers\Api;

use App\User;
use Dingo\Api\Http\Request;
use App\Http\Requests;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserTransformer;
use Dingo\Api\Exception\NotFoundHttpException;


class UserController extends BaseController
{

    // public function __construct()
    // {
    //     $this->middleware('api.auth');
    // }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(4);

        return $this->response->paginator($users, new UserTransformer);

        //for only authenticated users
        // $user = $this->auth->user();

        // return $user;
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
        $user= User::findOrFail($id);
        
        if (!$user)
        {
            throw new NotFoundHttpException('this user does not exist');
        }
        else{
            return $this->response->item($user, new UserTransformer);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}

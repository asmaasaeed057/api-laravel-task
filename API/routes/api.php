<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$api = app('Dingo\Api\Routing\Router');

$api->version('v1',function ($api) {

    $api->get('users', ['as' => 'users.index', 'uses' => 'App\Http\Controllers\Api\UserController@index']);
    $api->get('users/{id}', ['as' => 'users.show', 'uses' => 'App\Http\Controllers\Api\UserController@show']);
});


$api->version('v1',function ($api) {
    $api->get('news', ['as' => 'news.index', 'uses' => 'App\Http\Controllers\Api\NewsController@index']);
    $api->get('news/{id}', ['as' => 'news.show', 'uses' => 'App\Http\Controllers\Api\NewsController@show']);
    $api->post('news' , ['as' => 'news.store', 'uses' => 'App\Http\Controllers\Api\NewsController@store']);
    $api->put('news/{id}', ['as' => 'news.update', 'uses' => 'App\Http\Controllers\Api\NewsController@update']);
    $api->delete('news/{id}', ['as' => 'news.destroy', 'uses' => 'App\Http\Controllers\Api\NewsController@destroy']);
});

$api->version('v1', function ($api) {

    $api->post('login', 'App\Http\Controllers\Auth\LoginController@login');
    $api->post('register', 'App\Http\Controllers\Auth\RegisterController@register');
   
   }); 

   

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
